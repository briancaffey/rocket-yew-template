# Rocket + Yew Project Template

This project sets up a development environment for full-stack web application using Rocket and Yew.

## Start the project locally

```
docker-compose up
```

